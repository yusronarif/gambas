#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: TreeView 3.6.0\n"
"PO-Revision-Date: 2014-10-10 13:53 UTC\n"
"Last-Translator: Willy Raets <willy@develop.earthshipeurope.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "TreeView example"
msgstr "TreeView voorbeeld"

#: TreeViewExample.form:20
msgid "TreeView Example"
msgstr "TreeView Voorbeeld"

#: TreeViewExample.form:25
msgid "Help"
msgstr "-"

#: TreeViewExample.form:28
msgid "About"
msgstr "Over"

#: TreeViewExample.form:40
msgid "Insert Name"
msgstr "Naam tussenvoegen"

#: TreeViewExample.form:45
msgid "Remove Name"
msgstr "Naam verwijderen"

#: TreeViewExample.form:54
msgid "Current item"
msgstr "Huidig item"

#: TreeViewExample.form:64
msgid "Events"
msgstr "Gebeurtenissen"

#: TreeViewExample.form:74
msgid "&Clear events"
msgstr "&Gebeurtenissen opschonen"

#: TreeViewExample.form:79
msgid "Male"
msgstr "Mannelijk"

#: TreeViewExample.form:85
msgid "Female"
msgstr "Vrouwelijk"

#: TreeViewExample.class:42
msgid "&1 has &2 children."
msgstr "&1 heeft &2 kinderen."

#: TreeViewExample.class:44
msgid "&1 has no children."
msgstr "&1 heeft geen kinderen."

#: TreeViewExample.class:46
msgid "&1 has 1 child."
msgstr "&1 heeft 1 kind."

#: TreeViewExample.class:49
msgid "<br>Item rect is ("
msgstr "<br>Item rect is ("

#: TreeViewExample.class:150
msgid "TreeView example written by C. Packard and Fabien Hutrel."
msgstr "TreeView voorbeeld geschreven door C. Packard en Fabien Hutrel."

